import web as w

texto = w.leerURL()

def textoHTMLlista(texto):
    sub = 0
    sub2 = 1
    frase, palabra = '', ''
    lista = []
    while sub2 != -1:
        sub = texto.find('>',sub + 1)
        sub2 = texto.find('<', sub2 + 1)
        frase += texto[sub+1:sub2]
    for ch in frase:
        if ch.isalpha():
            palabra += ch
        else:
            if palabra.isalpha():
                lista.append(palabra)
                palabra = ''
    if palabra != '':
        lista.append(palabra)
    return lista

print(len(textoHTMLlista(texto)))
