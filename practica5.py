import web as w

contenido = w.leerURL()

def eliminar(contenido):
    """
    Retorna un String, con las etiquetas HTML eleminadas del contenido de la web.
    :param contenido: String, contenido leido de una web
    :return: String
    """
    sub = 0
    sub2 = 1
    frase = ' '
    while sub2 != -1:
        sub = contenido.find('>',sub + 1)
        sub2 = contenido.find('<', sub2 + 1)
        frase += contenido[sub+1:sub2]
    return frase


texto = eliminar(contenido)

# def contarPalabras(texto):
#
#     p = ''
#     contador = 0
#     for ch in texto:
#         if ch != ' ':
#             if ch.isalpha():
#                 p += ch
#         else:
#             print(p)
#             p = ' '
#             contador += 1
#     if p != ' ':
#         contador += 1
#         print(p)
#     print(f'Tenemos {contador} palabras contadas en el texto.')

#contarPalabras(texto)


def contarPalabras(texto):
     p = ''
     contador = 0
     for ch in texto:
        if ch.isalpha():
            p += ch
        else:
            if p.isalpha():
                print(p)
                p = ''
                contador += 1
     print(f'El texto contiene {contador} palabras.')

contarPalabras(texto)