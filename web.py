# -*- coding: utf-8 -*-
"""
web.py

Este módulo ofrece funciones para trabajar con URLs de páginas web.
@author: FPR-CD
"""

import requests


def leerContenidoURL(url):
    """
    Devuelve una cadena de texto con el contenido de la página con la URL
    indicada. Devuelve None si no se puede recuperar el contenido de dicha
    página, porque no existe o porque no es accesible.   
    """
    try:
        r = requests.get(url, timeout = 10)
    except requests.exceptions.RequestException:
        return None
    if r.status_code != requests.codes.ok:
        return None
    r.encoding = r.apparent_encoding
    return r.text


def leerURL():
    while True:
        try:
            url = 'http://personales.upv.es/ossaver/fpr/practica5.htm'#input('Escribe una URL o direccion web valida...')
            contenido = leerContenidoURL(url)
            if contenido != None:
               return contenido
        except:
            print('Debes introducir los caracteres correctos.')
